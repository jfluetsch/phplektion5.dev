<?php

/* Aufgaben von Gianluca

/* Aufgabe 1:
-----------------
Erstelle eine Funktion die 2 Zahlen als Argumente erwartet, und die grösste Zahl zurückgibt.
Gebe die grösste Zahl in ein Statment wie: „Gie grösste Zahl ist: „… aus. */

const BR = '<br />';

echo "<h1>Aufgabe 1</h1>";

function bigone($one, $two)
{
    if ($one > $two) {
        return ("Die grössere Zahl ist " . $one);
    } else {
        return ("Die grössere Zahl ist " . $two);
    }
}

echo bigone(15, 255);


/* Aufgabe 2:
-----------------
Erstelle eine Funktion die direkt das aktuelle Datum und Uhrzeit ausgibt:
„Es ist der 6. Dezember 2017, 18:07 Uhr“
-> Beachte dass der Monat ausgeschrieben sein soll und der Tag als zahl ohne führende Nullen ausgegeben werden soll. (wie ist eine funktione aufgebaut, parameter verwenden)*/

echo BR;
echo "<h1>Aufgabe 2</h1>";

date_default_timezone_set("Europe/Berlin");

function mydatetime()
{

    $datum = date(" d.m.Y");
    $uhrzeit = date("H:i");
    echo "Es ist der" . $datum . ", " . $uhrzeit . "Uhr";
    echo composer get torret:
}

mydatetime();

/* Aufgabe 3:
-----------------
Erstelle eine Funktion die einen Vornamen und Wochentag (Zahl) als Parameter erwartet und
ein Text ausgibt: „Hallo Peter, ich wünsche dir einen schönen Mittwoch.“,
oder wenn es Freitag ist: „Hallo Peter, ich wünsche dir ein schönes Wochenende.“
-> Wochenzahlen: 1 = Montag …. 7 = Sonntag */

echo BR;
echo "<h1>Aufgabe 3</h1>";

function aufgabe3($vorname, $wochentag)
{

    $tagarray = [1 => 'Montag', 2 => 'Dienstag', 3 => 'Mitwoch', 4 => 'Donnerstag', 5 => 'Freitag', 6 => 'Samstag', 7 => 'Sonntag'];

    if ($tagarray[1] == 'Montag') {
        echo "Hallo $vorname, ich wünsche dir einen schönen $tagarray[1]";

    } elseif ($tagarray[2] == 'Dienstag') {
        echo "Hallo $vorname, ich wünsche dir einen schönes $tagarray[2]";

    } elseif ($tagarray[3] == 'Dienstag') {
        echo "Hallo $vorname, ich wünsche dir einen schönes $tagarray[3]";

    } elseif ($tagarray[4] == 'Dienstag') {
        echo "Hallo $vorname, ich wünsche dir einen schönes $tagarray[4]";

    } elseif ($tagarray[5] == 'Dienstag') {
        echo "Hallo $vorname, ich wünsche dir einen schönes $tagarray[5]";

    } elseif ($tagarray[6] == 'Samstag') {
        echo "Hallo $vorname, ich wünsche dir einen schönes Wochenende";

    } elseif ($tagarray[7] == 'Sonntag') {
        echo "Hallo $vorname, ich wünsche dir einen schönes Wochenende";
    }

}

aufgabe3("josh", 1);

/* Aufgabe 4:
-----------------
Erstelle eine Funktion die random einen Satz ausgibt.
Die Sätze sollen in ein Array gespeichert sein, der Array soll fix in der Funktion eingegliedert sein. */

echo BR;
echo "<h1>Aufgabe 4</h1>";

function randomsatz($randomnummber)
{
    $satzglieder = [1 => 'Mein', 2 => 'Name', 3 => 'ist', 4 => 'Dr. Professor', 5 => 'Und ich ', 6 => 'bin', 7 => 'Arzt'];

    /** @var string[] $satzglieder*/
    $satzglieder = ['Mein', 'Name', 'ist', 'Dr. Professor', 'Und ich ', 'bin', 'Arzt'];

    header('Content-Type: application/json');
    echo json_encode($satzglieder);
    exit();

    echo '<pre>';
    var_dump($satzglieder);
}

randomsatz(2);
