<?php
/**
 * Created by PhpStorm.
 * User: joshuahflutsch
 * Date: 13.12.17
 * Time: 17:02
 */

class System
{
    public function __construct()
    {
        date_default_timezone_set("Europe/Berlin");
    }

    public function mydatetime()
    {
        $date = new DateTime();
        $datum = date(" d.m.Y");
        $uhrzeit = date("H:i");
        echo "Es ist der" . $date->format("d.m.Y") . ", " . $uhrzeit->format("H:i");
    }

    /**
     * @param int $one
     * @param int $two
     * @return string
     */

    public function bigone($one, $two)
    {
        return "Die grössere Zahl ist " . ($one > $two ? $one : $two);
    }

}