<?php

/* Aufgaben von Gianluca

/* Aufgabe 1:
-----------------
Erstelle eine Funktion die 2 Zahlen als Argumente erwartet, und die grösste Zahl zurückgibt.
Gebe die grösste Zahl in ein Statment wie: „Gie grösste Zahl ist: „… aus. */

const BR = '<br />';

echo "<h1>Aufgabe 1</h1>";

function bigone($one, $two)
{
    if ($one > $two) {
        return ("Die grössere Zahl ist " . $one);
    } else {
        return ("Die grössere Zahl ist " . $two);
    }
}

echo bigone(25, 50);


/* Aufgabe 2:
-----------------
Erstelle eine Funktion die direkt das aktuelle Datum und Uhrzeit ausgibt:
„Es ist der 6. Dezember 2017, 18:07 Uhr“
-> Beachte dass der Monat ausgeschrieben sein soll und der Tag als zahl ohne führende Nullen ausgegeben werden soll. (wie ist eine funktione aufgebaut, parameter verwenden)*/

echo BR;
echo "<h1>Aufgabe 2</h1>";

date_default_timezone_set("Europe/Berlin");

function mydatetime()
{

    $datum = date("d.m.Y");
    $uhrzeit = date("H:i");
    echo "Es ist der" . $datum . ", " . $uhrzeit . "Uhr";
}



/* Aufgabe 3:
-----------------
Erstelle eine Funktion die einen Vornamen und Wochentag (Zahl) als Parameter erwartet und
ein Text ausgibt: „Hallo Peter, ich wünsche dir einen schönen Mittwoch.“,
oder wenn es Freitag ist: „Hallo Peter, ich wünsche dir ein schönes Wochenende.“
-> Wochenzahlen: 1 = Montag …. 7 = Sonntag */

echo BR;
echo "<h1>Aufgabe 3</h1>";

function aufgabe3($vorname, $wochentag)
{

    $tagarray = [1 => 'Montag', 2 => 'Dienstag', 3 => 'Mitwoch', 4 => 'Donnerstag', 5 => 'Freitag', 6 => 'Samstag', 7 => 'Sonntag'];

    if ($tagarray[1] == 'Montag') {
        echo "Hallo $vorname, ich wünsche dir einen schönen $tagarray[1]";

    } elseif ($tagarray[2] == 'Dienstag') {
        echo "Hallo $vorname, ich wünsche dir einen schönes $tagarray[2]";

    } elseif ($tagarray[3] == 'Dienstag') {
        echo "Hallo $vorname, ich wünsche dir einen schönes $tagarray[3]";

    } elseif ($tagarray[4] == 'Dienstag') {
        echo "Hallo $vorname, ich wünsche dir einen schönes $tagarray[4]";

    } elseif ($tagarray[5] == 'Dienstag') {
        echo "Hallo $vorname, ich wünsche dir einen schönes $tagarray[5]";

    } elseif ($tagarray[6] == 'Samstag') {
        echo "Hallo $vorname, ich wünsche dir einen schönes Wochenende";

    } elseif ($tagarray[7] == 'Sonntag') {
        echo "Hallo $vorname, ich wünsche dir einen schönes Wochenende";
    }

}
aufgabe3("josh", 1);

/* Aufgabe 4:
-----------------
Erstelle eine Funktion die random einen Satz ausgibt.
Die Sätze sollen in ein Array gespeichert sein, der Array soll fix in der Funktion eingegliedert sein. */

echo BR;
echo "<h1>Aufgabe 4</h1>";

function randomsatz()
{
    $satzglieder = [1 => 'Mein Name ist Josh.', 2 => 'Was für ein Satz.', 3 => 'Das ist auch ein Satz!', 4 => 'Die Aufgabe wurde richtig gelöst', 5 => 'Anscheinend hast du diese Aufgabe verstanden.', 6 => 'Nocheinmal irgend ein Satz', 7 => 'Anschliessend noch der letzten Satz.'];

    return $satzglieder [rand(1,7)];

}
    echo randomsatz();


/* Aufgabe 5:
-----------------
Erstelle eine Funktion die checkt ob ein Benutzername und Passwort korrekt eingegeben wurden,
und gib „true“ zurück, wenn diese übereinstimmen.
Der Benutzername und Passwort kann fix in der Funktion definiert werden.
*/

echo BR;
echo "<h1>Aufgabe 5</h1>";

function checkUser($username, $password) {



    if ($username == "josh123") {
        return "true";
    } elseif ($password == "pass4josh") {
        return "true";
    } else {
        return "false";
    }

}

echo checkUser("josh123", "pass4josh");
