<?php
require_once 'vendor/autoload.php';

$config = new \Doctrine\DBAL\Configuration();
$connectionParams = array(
    'dbname' => 'lektion5',
    'user' => 'root',
    'password' => 'root',
    'host' => 'localhost',
    'driver' => 'pdo_mysql',
);

$db = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);



$loader = new Twig_Loader_Filesystem('web/templates/'); // Loads Templates from filesystem
$twig = new Twig_Environment($loader, array ( // Settings for twig enviroment
    'debug' => true,
    // 'cache' => __DIR__.'/cache/',
));
$twig->addExtension(new Twig_Extension_Debug());

// url = irgendwas.dev/?page=home
$page = $_GET['page'];

// To do till Monday (Gianluca) // Fallback wenn der Wert für page nichr existiert. - home
if ($page != true) {
    $page = 'home';
}

// To do till Monday (Gianluca) // Variable filename beinhalten den pfad und die datei die benötigt wird.
$filename = 'web/templates/'. $page.'.twig';
if (file_exists($filename) == false) { // wenn das file nicht existiert wird auf die 404 Seite zurückgegriffen - 404 Fallback
    $page = '404';
}

$sql = "SELECT * FROM pages WHERE page_name= '".$page."' ";
$content = $db->query($sql)->fetch();

$sql = "SELECT page_name, navigation_title FROM pages WHERE page_parent IS NULL";
$navigation = $db->query($sql)->fetchAll();


$data = '';
$searchfrase = '';

// all items
if ( $page == 'alle') {
    // call all album
    $sql = "SELECT * FROM alben  ";
    $data = $db->query($sql)->fetchAll();
} elseif ($page == 'detail') {
    $albumid = $_GET['albumid']; //
    $sql = "SELECT * FROM alben WHERE id= '".$albumid."'"; // String
    $data = $db->query($sql)->fetch(); // SQL - Query
} elseif ($page == 'edit') {

    if ( isset($_POST['action']) && $_POST['action'] == 'delete' ) {

        // remove data from db
        $db->delete('alben', array("id" => $_POST['postid']));
        print_r($_POST);

    }

    if ( isset($_POST['action']) && $_POST['action'] == 'update' ) {

        // save data in db
        $updatedata['album_name'] = $_POST['album-name'];
        $updatedata['name_band'] = $_POST['band-name'];
        $updatedata['album_content'] = $_POST['album-content'];
        $updatedata['publishing_year'] = $_POST['publishing-year'];
        $db->update('alben', $updatedata, array("id" => $_POST['postid']));

        print_r($_POST);

    }
    $albumid = $_GET['albumid'];
    $sql = "SELECT * FROM alben WHERE id= '" . $albumid . "'"; // String
    $data = $db->query($sql)->fetch(); // SQL - Query
} elseif ( $page == 'neu' ) {

    print_r($_POST);

    if ( isset($_POST['action']) && $_POST['action'] == 'insert' ) {

        // save data in db
        $insertdata['album_name'] = $_POST['album-name'];
        $insertdata['name_band'] = $_POST['band-name'];
        $insertdata['album_content'] = $_POST['album-content'];
        $insertdata['publishing_year'] = $_POST['publishing-year'];
        $db->insert('alben', $insertdata);

    }

} elseif ( $page == 'edit' ) {


  if ( isset($_POST['action']) && $_POST['action'] == 'delete' ) {

        // remove data from db
        //$db->delete('alben', array("id" => $_POST['postid']));
        //echo 'hello';
        //print_r($_POST);

  }
     $albumid = $_GET['albumid'];
     $sql = "SELECT * FROM alben WHERE id= '" . $albumid . "'"; // String
     $data = $db->query($sql)->fetch(); // SQL - Query

} elseif ( $page == 'search' ) {


    if ( isset($_POST['action']) && $_POST['action'] == 'search' ) {

        // remove data from db
        //$db->delete('alben', array("id" => $_POST['postid']));
        // print_r($_POST);

        $searchfrase = $_POST['search'];
        $sql = "SELECT * FROM alben 
              WHERE 
                album_name LIKE '%" . $searchfrase . "%' 
                OR name_band LIKE '%" . $searchfrase . "%'
                OR publishing_year LIKE '%" . $searchfrase . "%'
                "; // String
        $data = $db->query($sql)->fetchAll(); // SQL - Query


    }


}

echo $twig->render( // Ausgabe der aktuellen Seite (Template)
    $page.'.twig',[
    'page' => $page,
    'content' => $content,
    'navigation' => $navigation,
    'data' => $data,
    'searchfrase' => $searchfrase
]);

