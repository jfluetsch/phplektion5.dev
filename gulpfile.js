var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var gulpif = require('gulp-if');
var util = require('gulp-util');
var plumber = require('gulp-plumber');
var browserSync = require('browser-sync').create();
var uglify = require('gulp-uglify');
var gulpCopy = require('gulp-copy');
var babel = require('gulp-babel');



var config = {
    assetsDir: 'assets',
    sassPattern: 'scss/**/*.scss',
    scriptsPattern: 'scripts/**/*.js',
    production: !!util.env.production,
    sourceMaps: !util.env.production,
    bowerDir: 'vendor/bower_components'
};



gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "http://project-josh.local/"
    });
});

gulp.task('images', function () {
    gulp.src(config.assetsDir+'/images/*')
        .pipe(gulp.dest('images/'));
});

gulp.task('styles', function() {
    gulp.src([
        config.assetsDir+'/scss/main.scss'
    ])
        .pipe(plumber())
        .pipe(gulpif(config.sourceMaps, sourcemaps.init()))
        .pipe(sass())
        .pipe(concat('main.css'))
        .pipe(cleanCSS())
        .pipe(gulpif(config.sourceMaps, sourcemaps.write('.')))
        .pipe(gulp.dest('css'))
        .pipe(browserSync.stream());
});

gulp.task('babel', function() {
    return gulp.src(config.assetsDir+'/es6/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat('scripts_es6.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.assetsDir+'/scripts/'));
});

gulp.task('scripts', function() {
    gulp.src([
        config.bowerDir+'/jquery/dist/jquery.min.js',
        config.assetsDir+'/scripts/**/*.js'
    ])
        .pipe(plumber())
        .pipe(gulpif(config.sourceMaps, sourcemaps.init()))
        .pipe(concat('scripts.js'))
        .pipe(gulpif(config.sourceMaps, sourcemaps.write('.')))
        .pipe(gulp.dest('js'))
        .pipe(browserSync.stream());
});

gulp.task('watch', function() {
    gulp.watch(config.assetsDir+'/'+config.sassPattern, ['styles']);
    gulp.watch(config.assetsDir+'/'+config.scriptsPattern, ['scripts']);
});

gulp.task('default', ['styles', 'babel', 'scripts', 'images', 'browser-sync', 'watch']);
